const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/example')
const Building = require('./models/Building')
const Room = require('./models/Room')

async function main () {
  // Update
  // const room = await Room.findById('621a1c65a5d5f2a3a8fdb918')
  // room.capacity = 20
  // room.save()

  // const room = await Room.findOne({ _id: '621a1c65a5d5f2a3a8fdb918' })
  // const room = await Room.find({ capacity: { $gte: 100 } })
  const room = await Room.findOne({ capacity: { $gte: 100 } }).populate('building')
  console.log(room)
  const rooms = await Room.find({ capacity: { $gte: 100 } }).populate('building')
  console.log(rooms)
  const building = await Building.find({}).populate('rooms')
  console.log(JSON.stringify(building))
}

main().then(function () {
  console.log('Finish')
})
