const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/example')
const Room = require('./models/Room')
const Building = require('./models/Building')

async function main () {
  const newInformaticBuilding = await Building.findById('621a1c65a5d5f2a3a8fdb922')
  const room = await Room.findById('621a1c65a5d5f2a3a8fdb91a')
  const informaticBuilding = await Building.findById(room.building)
  console.log(newInformaticBuilding)
  console.log(room)
  console.log(informaticBuilding)
  room.building = newInformaticBuilding
  newInformaticBuilding.rooms.push(room)
  informaticBuilding.rooms.pull(room)
  room.save()
  informaticBuilding.save()
  newInformaticBuilding.save()
}

main().then(function () {
  console.log('Finish')
})
