const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/example')
const Room = require('./models/Room')
const Building = require('./models/Building')

async function clearDb () {
  await Room.deleteMany({})
  await Building.deleteMany({})
}

async function main () {
  await clearDb()
  const informaticBuilding = new Building({ name: 'Informatic', floor: 11 })
  const room3c01 = new Room({ name: '3c01', capacity: 200, floor: 3, building: informaticBuilding })
  const room4c01 = new Room({ name: '4c01', capacity: 150, floor: 4, building: informaticBuilding })
  const room5c01 = new Room({ name: '5c01', capacity: 100, floor: 5, building: informaticBuilding })
  informaticBuilding.rooms.push(room3c01)
  informaticBuilding.rooms.push(room4c01)
  informaticBuilding.rooms.push(room5c01)
  await informaticBuilding.save()
  await room3c01.save()
  await room4c01.save()
  await room5c01.save()

  const newInformaticBuilding = new Building({ name: 'New Informatic', floor: 20 })
  await newInformaticBuilding.save()
}

main().then(function () {
  console.log('Finish')
})
